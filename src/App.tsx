import "./App.css";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";

import { Home } from "./pages";
import { CameraProvider } from "./context/CameraContext.jsx";

const App = () => {
  return (
    <>
      <main className="bg-slate-300/20 overflow-hidden">
        <CameraProvider>
          <Router>
            <Routes>
              <Route path="/" element={<Home />} />
            </Routes>
          </Router>
        </CameraProvider>
      </main>
    </>
  );
};

export default App;
