import planeScene from "../assets/3d/plane.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { useEffect, useRef } from "react";

const Plane = () => {
  const ref = useRef();
  const { scene, animations } = useGLTF(planeScene);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played
    if (actions && actions["Scene"]) {
      const action = actions["Scene"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh position={[-200, 200, 850]} scale={[0.1, 0.1, 0.1]} ref={ref}>
      <primitive object={scene} />
    </mesh>
  );
};

export default Plane;
