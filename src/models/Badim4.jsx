import Badim4GLB from "../assets/3d/batim4.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { forwardRef, useEffect } from "react";

const Badim1 = forwardRef(({ islandRef }, ref) => {
  const { scene, animations } = useGLTF(Badim4GLB);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played
    if (actions && actions["mixamo.com"]) {
      const action = actions["mixamo.com"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh
      position={[2650, -60, 1000]}
      scale={[30, 30, 30]}
      ref={ref}
      rotation={[0, 10, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Badim1;
