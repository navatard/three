import Badim2GLB from "../assets/3d/batim2.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { forwardRef, useEffect } from "react";

const Badim2 = forwardRef(({ islandRef }, ref) => {
  const { scene, animations } = useGLTF(Badim2GLB);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    if (actions && actions["ch4sammy_Boris_Other_Axe_skeleton|idle"]) {
      const action = actions["ch4sammy_Boris_Other_Axe_skeleton|idle"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh
      position={[2500, -50, 1100]}
      scale={[100, 100, 100]}
      ref={ref}
      rotation={[0, 10, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Badim2;
