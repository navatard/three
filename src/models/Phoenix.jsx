import PhoenixGLB from "../assets/3d/phoenix.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { useFrame } from "@react-three/fiber";
import { forwardRef, useEffect, useRef } from "react";

const Phoenix = forwardRef(() => {
  const { scene, animations } = useGLTF(PhoenixGLB);
  const birdRef = useRef();
  const { actions } = useAnimations(animations, birdRef);
  const angleRef = useRef(0);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played

    if (actions && actions["Take 001"]) {
      const action = actions["Take 001"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  useFrame((state, delta) => {
    // Increment the angle
    angleRef.current += delta;

    // Define the radius and speed of the circle
    const radius = 2000; // Adjust the radius as needed
    const speed = 0.5; // Adjust the speed as needed

    // Calculate the new position
    const x = radius * Math.cos(speed * angleRef.current);
    const z = radius * Math.sin(speed * angleRef.current);
    const y = 2000; // Fixed height above the island

    // Update the bird's position
    if (birdRef.current) {
      birdRef.current.position.set(x, y, z);
      const directionAngle = Math.atan2(z, x);
      birdRef.current.rotation.y = directionAngle + Math.PI / 5;
    }
  });

  return (
    <mesh
      position={[0, 2000, 0]}
      scale={[1, 1, 1]}
      ref={birdRef}
      rotation={[0, 0, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Phoenix;
