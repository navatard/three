import SpiderGLB from "../assets/3d/spider.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { forwardRef, useEffect } from "react";

const Spider = forwardRef(({ islandRef }, ref) => {
  const { scene, animations } = useGLTF(SpiderGLB);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played
    if (actions && actions["Arachne"]) {
      const action = actions["Arachne"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh
      position={[100, -200, -7200]}
      scale={[250, 250, 250]}
      ref={ref}
      rotation={[0, 43.9, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Spider;
