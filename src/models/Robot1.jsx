import Robot1GLB from "../assets/3d/robot1.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { forwardRef, useEffect } from "react";

const Robot1 = forwardRef(({ islandRef }, ref) => {
  const { scene, animations } = useGLTF(Robot1GLB);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played
    if (actions && actions["SK_StaffBot_FizzyFaz.ao|DefaultSlot.011"]) {
      const action = actions["SK_StaffBot_FizzyFaz.ao|DefaultSlot.011"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh
      position={[-1800, 0, 2100]}
      scale={[200, 200, 200]}
      ref={ref}
      rotation={[0, Math.PI / 2, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Robot1;
