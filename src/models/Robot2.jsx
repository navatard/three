import Robot2GLB from "../assets/3d/robot2.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { forwardRef, useEffect } from "react";

const Robot1 = forwardRef(({ islandRef }, ref) => {
  const { scene, animations } = useGLTF(Robot2GLB);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played
    if (actions && actions["Animation"]) {
      const action = actions["Animation"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh
      position={[-1800, 0, 1850]}
      scale={[200, 200, 200]}
      ref={ref}
      rotation={[0, 20, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Robot1;
