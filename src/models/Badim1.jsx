import Badim1GLB from "../assets/3d/batim1.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { forwardRef, useEffect } from "react";

const Badim1 = forwardRef(({ islandRef }, ref) => {
  const { scene, animations } = useGLTF(Badim1GLB);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played

    if (
      actions &&
      actions["betach3bendy_beta_bendy_bendy_Model_skeleton|search"]
    ) {
      const action =
        actions["betach3bendy_beta_bendy_bendy_Model_skeleton|search"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh
      position={[2800, 30, 1200]}
      scale={[2, 2, 2]}
      ref={ref}
      rotation={[0, 30, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Badim1;
