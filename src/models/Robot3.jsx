import Robot3GLB from "../assets/3d/robot3.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { forwardRef, useEffect } from "react";

const Robot1 = forwardRef(({ islandRef }, ref) => {
  const { scene, animations } = useGLTF(Robot3GLB);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played
    if (actions && actions["metarig|metarigAction"]) {
      const action = actions["metarig|metarigAction"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh
      position={[-2000, 430, 1550]}
      scale={[100, 100, 100]}
      ref={ref}
      rotation={[0, 70, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Robot1;
