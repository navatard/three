import Badim3GLB from "../assets/3d/batim3.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { forwardRef, useEffect } from "react";

const Badim3 = forwardRef(({ islandRef }, ref) => {
  const { scene, animations } = useGLTF(Badim3GLB);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played
    // Ensure the "Scene" animation action is accessed and played
    if (actions && actions["bruteboris_ACH_BONE_skeleton|hittable"]) {
      const action = actions["bruteboris_ACH_BONE_skeleton|hittable"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh
      position={[2900, -50, 900]}
      scale={[4, 4, 4]}
      ref={ref}
      rotation={[0, 42, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Badim3;
