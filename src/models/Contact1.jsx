import ContactGLB from "../assets/3d/contact1.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { forwardRef, useEffect } from "react";

const Contact1 = forwardRef(({ islandRef }, ref) => {
  const { scene, animations } = useGLTF(ContactGLB);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played
    if (actions && actions["Animation"]) {
      const action = actions["Animation"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh
      position={[-1200, 0, -200]}
      scale={[5, 5, 5]}
      ref={ref}
      rotation={[0, 43.2, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Contact1;
