import fisherScene from "../assets/3d/fisherman.glb";
import { useAnimations, useGLTF } from "@react-three/drei";
import { forwardRef, useEffect } from "react";

const Fisher = forwardRef(({ islandRef }, ref) => {
  const { scene, animations } = useGLTF(fisherScene);

  const { actions } = useAnimations(animations, ref);

  useEffect(() => {
    // Ensure the "Scene" animation action is accessed and played
    if (actions && actions["Armature|idol"]) {
      const action = actions["Armature|idol"];
      action.play();
    } else {
      console.warn('No "Scene" animation found.');
    }
  }, [actions]);

  return (
    <mesh
      position={[-1800, 0, 2200]}
      scale={[50, 50, 50]}
      ref={ref}
      rotation={[0, Math.PI / 2, 0]}
    >
      <primitive object={scene} />
    </mesh>
  );
});

export default Fisher;
