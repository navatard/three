import { NavLink } from "react-router-dom";
import { useCamera } from "../context/CameraContext.jsx";

export const HeaderInfo = ({
  text,
  targetPosition,
  targetLookAt,
  setIsStageFocus,
  setDarkTheme,
  setIsIslandLightlyRotating,
  setRotateValue,
  rotateValue,
}) => {
  const { setTargetPositionWithDelay } = useCamera();

  const handleClick = () => {
    setTargetPositionWithDelay(targetPosition, targetLookAt, 0);
    setIsStageFocus(true);
    setIsIslandLightlyRotating(true);
    setRotateValue(rotateValue);
    if (setDarkTheme) {
      setDarkTheme(true);
    }
  };

  return (
    <p className="cursor-pointer" onClick={handleClick}>
      {text}
    </p>
  );
};

const Navbar = ({
  setIsStageFocus,
  setIsIslandLightlyRotating,
  setRotateValue,
  setDarkTheme,
}) => {
  return (
    <header className="header !z-50 relative">
      <NavLink
        to="/"
        className="py-2 px-4 rounded-lg bg-white items-center justify-center flex font-bold shadow-md"
      >
        <p className="blue-gradient_text">MATHIEU BERCHI</p>
      </NavLink>
      <nav className="flex text-lg gap-7 font-medium">
        <HeaderInfo
          targetPosition={{ x: -100, y: 50, z: 700 }} //{ x: 850, y: 50, z: 650 }
          targetLookAt={[-800, 0, 1400]} // [-1800, 0, 2200]
          rotateValue={-18.5}
          setRotateValue={setRotateValue}
          text="Skills"
          setIsStageFocus={setIsStageFocus}
          setIsIslandLightlyRotating={setIsIslandLightlyRotating}
        />
        <HeaderInfo
          targetPosition={{ x: 300, y: 0, z: 600 }}
          targetLookAt={[-1500, 100, 3500]}
          rotateValue={-19.7}
          setRotateValue={setRotateValue}
          text="Experiences"
          setIsStageFocus={setIsStageFocus}
          setIsIslandLightlyRotating={setIsIslandLightlyRotating}
        />
        <HeaderInfo
          targetPosition={{ x: 400, y: -150, z: 1850 }}
          targetLookAt={[1800, 200, 4200]}
          rotateValue={-21.7}
          setRotateValue={setRotateValue}
          text="Portfolio"
          setIsStageFocus={setIsStageFocus}
          setIsIslandLightlyRotating={setIsIslandLightlyRotating}
          setDarkTheme={setDarkTheme}
        />
        <HeaderInfo
          targetPosition={{ x: 300, y: 20, z: 300 }}
          targetLookAt={[-1000, 0, 400]}
          rotateValue={-22.8}
          setRotateValue={setRotateValue}
          text="Contact"
          setIsStageFocus={setIsStageFocus}
          setIsIslandLightlyRotating={setIsIslandLightlyRotating}
        />
      </nav>
    </header>
  );
};

export default Navbar;
