import { motion } from "framer-motion";
import Back from "../../assets/3d/back.png";

const Skills = ({ handleClick }) => {
  return (
    <div className="absolute top-56 left-24 z-30 flex items-center justify-end">
      <motion.div
        initial={{ opacity: 0, y: 20 }}
        animate={{
          opacity: 1,
          y: 0,
          transition: { delay: 1.3 }, // Set the delay for the enter transition
        }}
        exit={{
          opacity: 0,
          y: 20,
          transition: { delay: 0 }, // Remove the delay for the exit transition
        }}
        className="info-box bg-blue-fade "
      >
        <img
          src={Back}
          className="absolute top-5 left-10 cursor-pointer"
          onClick={handleClick}
        />
        <h2 className="text-center text-xl font-medium">Skills</h2>
        <p className="font-light text-center">
          Here is a panel of skills that I was able to master <br />
          during my professional career.
        </p>
        <ul className="flex flex-wrap">
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 1.5 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            PHP
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 1.6 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            Wordpress
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 1.7 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            MySQL
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 1.8 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            Javascript
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 1.9 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            jQuery
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 2 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            React.js
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 2.1 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            Typescript
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 2.2 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            React native
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 2.3 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            CSS / SCSS
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 2.4 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            Tailwind CSS
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 2.5 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            Styled components
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 2.6 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-32 mb-5"
          >
            Github / Gitlab
          </motion.li>
        </ul>
      </motion.div>
    </div>
  );
};

export default Skills;
