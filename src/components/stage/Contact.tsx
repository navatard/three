import { motion } from "framer-motion";
import Back from "../../assets/3d/back.png";

const Contact = ({ handleClick }) => {
  return (
    <div className="absolute top-80 left-24 z-30 flex items-center justify-end">
      <motion.div
        initial={{ opacity: 0, y: 20 }}
        animate={{
          opacity: 1,
          y: 0,
          transition: { delay: 1.3 }, // Set the delay for the enter transition
        }}
        exit={{
          opacity: 0,
          y: 20,
          transition: { delay: 0 }, // Remove the delay for the exit transition
        }}
        className="info-box bg-blue-fade"
      >
        <img
          src={Back}
          className="absolute top-5 left-10 cursor-pointer"
          onClick={handleClick}
        />
        <h2 className="text-center text-xl font-medium">Contact</h2>
        <ul className="flex flex-wrap">
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 1.5 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-max mb-5 flex flex-col"
          >
            <span className="text-black font-medium">
              I am currently looking for a position in front-end development, do
              not hesitate to contact me by phone or email if you are interested
              in my profile.
            </span>
            <p className="text-black">
              <span className="blue">Phone :</span> <span>0618342490</span>
              <br />
              <br />
              <span className="blue">Email :</span>{" "}
              <span>mathieu.berchi@gmail.com</span>
            </p>
          </motion.li>
        </ul>
      </motion.div>
    </div>
  );
};

export default Contact;
