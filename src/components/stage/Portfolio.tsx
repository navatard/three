import { motion } from "framer-motion";
import Tesla from "../../assets/3d/tesla.jpg";
import JO from "../../assets/3d/jo.jpg";
import Fifa from "../../assets/3d/fifa.jpg";
import Nike from "../../assets/3d/nike.jpg";

const Portfolio = ({ handleClick }) => {
  return (
    <div className="absolute top-28 z-30 flex items-center justify-between w-[95%] mx-[2.5%]">
      <motion.div
        initial={{ opacity: 0, y: 20 }}
        animate={{
          opacity: 1,
          y: 0,
          transition: { delay: 1.3 }, // Set the delay for the enter transition
        }}
        exit={{
          opacity: 0,
          y: 20,
          transition: { delay: 0 }, // Remove the delay for the exit transition
        }}
        className="info-box bg-blue-fade !p-0 w-[23%]"
        onClick={handleClick}
      >
        <img
          src={Nike}
          className="h-[250px] rounded-md object-cover w-[100%]"
        />
        <div className="absolute w-[100%] h-[100%] bg-blue-fade flex items-center justify-center m-auto opacity-0 hover:opacity-100 transition duration-300 ease-in-out cursor-pointer">
          <h2 className="absolute text-center text-2xl font-medium">Nike</h2>
        </div>
      </motion.div>
      <motion.div
        initial={{ opacity: 0, y: 20 }}
        animate={{
          opacity: 1,
          y: 0,
          transition: { delay: 1.3 }, // Set the delay for the enter transition
        }}
        exit={{
          opacity: 0,
          y: 20,
          transition: { delay: 0 }, // Remove the delay for the exit transition
        }}
        className="info-box bg-blue-fade !p-0 w-[23%]"
        onClick={handleClick}
      >
        <img src={JO} className=" h-[250px] rounded-md object-cover w-[100%]" />
        <div className="absolute w-[100%] h-[100%] bg-blue-fade flex items-center justify-center m-auto opacity-0 hover:opacity-100 transition duration-300 ease-in-out cursor-pointer">
          <h2 className="absolute text-center text-2xl font-medium">
            JO Paris 2024
          </h2>
        </div>
      </motion.div>
      <motion.div
        initial={{ opacity: 0, y: 20 }}
        animate={{
          opacity: 1,
          y: 0,
          transition: { delay: 1.3 }, // Set the delay for the enter transition
        }}
        exit={{
          opacity: 0,
          y: 20,
          transition: { delay: 0 }, // Remove the delay for the exit transition
        }}
        className="info-box bg-blue-fade !p-0 w-[23%]"
        onClick={handleClick}
      >
        <img
          src={Tesla}
          className=" h-[250px] rounded-md object-cover w-[100%]"
        />
        <div className="absolute w-[100%] h-[100%] bg-blue-fade flex items-center justify-center m-auto opacity-0 hover:opacity-100 transition duration-300 ease-in-out cursor-pointer">
          <h2 className="absolute text-center text-2xl font-medium">Tesla</h2>
        </div>
      </motion.div>
      <motion.div
        initial={{ opacity: 0, y: 20 }}
        animate={{
          opacity: 1,
          y: 0,
          transition: { delay: 1.3 }, // Set the delay for the enter transition
        }}
        exit={{
          opacity: 0,
          y: 20,
          transition: { delay: 0 }, // Remove the delay for the exit transition
        }}
        className="info-box bg-blue-fade !p-0 w-[23%]"
        onClick={handleClick}
      >
        <img
          src={Fifa}
          className=" h-[250px] rounded-md object-cover w-[100%]"
        />
        <div className="absolute w-[100%] h-[100%] bg-blue-fade flex items-center justify-center m-auto opacity-0 hover:opacity-100 transition duration-300 ease-in-out cursor-pointer">
          <h2 className="absolute text-center text-2xl font-medium">Fifa</h2>
        </div>
      </motion.div>
    </div>
  );
};

export default Portfolio;
