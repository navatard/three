import { motion } from "framer-motion";
import Back from "../../assets/3d/back.png";

const Experiences = ({ handleClick }) => {
  return (
    <div className="absolute top-48 right-24 z-30 flex items-center justify-end">
      <motion.div
        initial={{ opacity: 0, y: 20 }}
        animate={{
          opacity: 1,
          y: 0,
          transition: { delay: 1.3 }, // Set the delay for the enter transition
        }}
        exit={{
          opacity: 0,
          y: 20,
          transition: { delay: 0 }, // Remove the delay for the exit transition
        }}
        className="info-box bg-blue-fade"
      >
        <img
          src={Back}
          className="absolute top-5 left-10 cursor-pointer"
          onClick={handleClick}
        />
        <h2 className="text-center text-xl font-medium">Experiences</h2>
        <p className="font-light text-center">
          Here are my main experiences in digital
        </p>
        <ul className="flex flex-wrap">
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 1.5 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-max mb-5 flex flex-col"
          >
            <span className="text-black">
              <span className="blue">Since October 2022</span> - Freelance
              Blockchain Front-end Developer (EOS / React.js)
            </span>
            <h3>Freelance, Europe</h3>
            <p className="text-black text-sm font-light">
              Participation in various projects ranging from Front end to
              blockchain, in an autonomous framework combining discipline and
              responsibility.
            </p>
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 1.7 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-max mb-5 flex flex-col"
          >
            <span className="text-black">
              <span className="blue">2020-2022</span> - Front-end Developer
              (React.js / React Native)
            </span>
            <h3>Skeepers / Hivency, Paris</h3>
            <p className="text-black text-sm font-light">
              Development and maintenance of two influencer marketing platforms,
              connecting brands and influencers for product promotion.
            </p>
          </motion.li>
          <motion.li
            initial={{ opacity: 0, y: 20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: { delay: 1.9 }, // Set the delay for the enter transition
            }}
            exit={{
              opacity: 0,
              y: 20,
              transition: { delay: 0 }, // Remove the delay for the exit transition
            }}
            className="neo-brutalism-white neo-btn !relative !w-max mb-5 flex flex-col"
          >
            <span className="text-black">
              <span className="blue">2017 -2020</span> - Web Developer (PHP /
              Javascript)
            </span>
            <h3>LATELIER, Nantes</h3>
            <p className="text-black text-sm font-light">
              Website design and tailor-made designs, with back office creation
              and optimization to facilitate maintenance of the site by the
              customer.
            </p>
          </motion.li>
        </ul>
      </motion.div>
    </div>
  );
};

export default Experiences;
