import { AnimatePresence, motion } from "framer-motion";
import { useCamera } from "../context/CameraContext.jsx";
import Skills from "./stage/Skills.js";
import Experiences from "./stage/Experiences.js";
import Portfolio from "./stage/Portfolio.js";
import Contact from "./stage/Contact.js";

const renderContent = (handleClick) => [
  <div></div>,
  <Skills handleClick={handleClick} />,
  <Experiences handleClick={handleClick} />,
  <Portfolio handleClick={handleClick} />,
  <Contact handleClick={handleClick} />,
  // add more stages as needed
];
interface StageInfoProps {
  currentStage: number;
  isStageFocus: boolean;
  setIsStageFocus: (focus: boolean) => void;
  setIsIslandLightlyRotating: (focus: boolean) => void;
  setDarkTheme: (focus: boolean) => void;
}
const StageInfo: React.FC<StageInfoProps> = ({
  currentStage,
  isStageFocus,
  setIsStageFocus,
  setIsIslandLightlyRotating,
  setDarkTheme,
}) => {
  const { setTargetPositionWithDelay } = useCamera();
  const handleClick = () => {
    setTargetPositionWithDelay({ x: 300, y: 300, z: 1100 }, [0, 0, 0], 0);
    setIsStageFocus(false);
    if (setDarkTheme) {
      setDarkTheme(false);
    }
    setTimeout(() => {
      setIsIslandLightlyRotating(false);
    }, 500);
  };
  return (
    <AnimatePresence>
      {isStageFocus && renderContent(handleClick)[currentStage]}
    </AnimatePresence>
  );
};

export default StageInfo;
