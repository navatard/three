import { useFrame } from "@react-three/fiber";
import { useEffect, useState, useRef } from "react";
import { useCamera } from "../context/CameraContext.jsx";
import * as THREE from "three"; // Import THREE from the three package

const CameraAnimation = ({ duration = 5000 }) => {
  const { targetPosition, animationDelay, targetLookAt } = useCamera();
  const [startAnimation, setStartAnimation] = useState(false);
  const startTimeRef = useRef(null);
  const initialQuaternion = useRef(new THREE.Quaternion());
  const targetQuaternion = useRef(new THREE.Quaternion());

  useEffect(() => {
    if (targetPosition && targetLookAt) {
      const targetPosVec = new THREE.Vector3(
        targetPosition.x,
        targetPosition.y,
        targetPosition.z
      );
      const lookAtVec = new THREE.Vector3(...targetLookAt);

      if (animationDelay !== 0) {
        const timer = setTimeout(() => {
          setStartAnimation(true);
          startTimeRef.current = performance.now();

          // Calculate target quaternion
          const targetMatrix = new THREE.Matrix4().lookAt(
            targetPosVec,
            lookAtVec,
            new THREE.Vector3(0, 1, 0)
          );
          targetQuaternion.current.setFromRotationMatrix(targetMatrix);
        }, animationDelay);
        return () => clearTimeout(timer);
      } else {
        setStartAnimation(true);
        startTimeRef.current = performance.now();

        // Calculate target quaternion
        const targetMatrix = new THREE.Matrix4().lookAt(
          targetPosVec,
          lookAtVec,
          new THREE.Vector3(0, 1, 0)
        );
        targetQuaternion.current.setFromRotationMatrix(targetMatrix);
      }
    }
  }, [targetPosition, animationDelay, targetLookAt]);

  const easeInOutCubic = (t) => {
    return t < 0.5 ? 4 * t * t * t : 1 - Math.pow(-2 * t + 2, 3) / 2;
  };

  useFrame((state) => {
    if (startAnimation && targetPosition) {
      if (!initialQuaternion.current.equals(state.camera.quaternion)) {
        initialQuaternion.current.copy(state.camera.quaternion);
      }

      const elapsed = performance.now() - startTimeRef.current;
      const t = Math.min(elapsed / duration, 1); // Normalized time between 0 and 1
      const easedT = easeInOutCubic(t); // Apply easing function

      // Interpolate camera position
      state.camera.position.lerpVectors(
        state.camera.position,
        new THREE.Vector3(targetPosition.x, targetPosition.y, targetPosition.z),
        easedT
      );

      // Interpolate camera rotation using quaternions
      state.camera.quaternion.slerpQuaternions(
        initialQuaternion.current,
        targetQuaternion.current,
        easedT
      );

      state.camera.updateProjectionMatrix();

      // Check if the animation is complete
      if (t === 1) {
        setStartAnimation(false);
      }
    }
  });

  return null;
};

export default CameraAnimation;
