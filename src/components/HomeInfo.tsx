import { useCamera } from "../context/CameraContext.jsx";
import Arrow from "../assets/3d/arrow.png";

interface InfoBoxProps {
  title: string;
  text: string;
  targetPosition: { x: number; y: number; z: number };
  targetLookAt: number[];
  btnText: string;
  isStageFocus: boolean;
  setIsStageFocus: (focus: boolean) => void;
  setDarkTheme?: (focus: boolean) => void;
  setIsIslandLightlyRotating: (focus: boolean) => void;
  setRotateValue: (value: number) => void;
  rotateValue: number;
  setTargetEffects?: () => {
    brightness: number;
    contrast: number;
    bloomIntensity: number;
    luminanceThreshold: number;
    luminanceSmoothing: number;
    vignetteDarkness: number;
  };
}

export const InfoBox: React.FC<InfoBoxProps> = ({
  title,
  text,
  targetPosition,
  targetLookAt,
  btnText,
  isStageFocus,
  setIsStageFocus,
  setDarkTheme,
  setIsIslandLightlyRotating,
  setRotateValue,
  rotateValue,
}) => {
  const { setTargetPositionWithDelay } = useCamera();

  const handleClick = () => {
    setTargetPositionWithDelay(targetPosition, targetLookAt, 0);
    setIsStageFocus(true);
    setIsIslandLightlyRotating(true);
    setRotateValue(rotateValue);
    if (setDarkTheme) {
      setDarkTheme(true);
    }
  };

  return (
    <div className={`${isStageFocus && "transition-fade"} info-box`}>
      <h2 className="text-center text-xl font-medium">{title}</h2>
      <p className=" font-light text-center">{text}</p>
      <button
        onClick={handleClick}
        className="neo-brutalism-white neo-btn cursor-pointer"
      >
        {btnText}
        <img className="w-5" src={Arrow} />
      </button>
    </div>
  );
};

const renderContent = (
  isStageFocus,
  setIsStageFocus,
  setDarkTheme,
  setIsIslandLightlyRotating,
  setRotateValue
) => [
  <div></div>,
  <InfoBox
    title="SKILLS"
    text="Thanks to a high attention to detail, constant questioning, and diligent technological monitoring, I was able to develop and improve my skills exponentially."
    targetPosition={{ x: -100, y: 50, z: 700 }} //{ x: 850, y: 50, z: 650 }
    targetLookAt={[-800, 0, 1400]} // [-1800, 0, 2200]
    rotateValue={-18.5}
    setRotateValue={setRotateValue}
    btnText="Learn more"
    isStageFocus={isStageFocus}
    setIsStageFocus={setIsStageFocus}
    setIsIslandLightlyRotating={setIsIslandLightlyRotating}
  />,
  <InfoBox
    title="EXPERIENCES"
    text="I had the chance to join companies that allowed me to acquire solid maturity and a greater sense of responsibility."
    targetPosition={{ x: 300, y: 0, z: 600 }}
    targetLookAt={[-1500, 100, 3500]}
    rotateValue={-19.7}
    setRotateValue={setRotateValue}
    btnText="Learn more"
    isStageFocus={isStageFocus}
    setIsStageFocus={setIsStageFocus}
    setIsIslandLightlyRotating={setIsIslandLightlyRotating}
  />,
  <InfoBox
    title="PORTFOLIO"
    text="
Here are some of my achievements, which have allowed me to be rewarded with awwwards, to be proud of my work and to satisfy my customers."
    targetPosition={{ x: 400, y: -150, z: 1850 }}
    targetLookAt={[1800, 200, 4200]}
    rotateValue={-21.7}
    setRotateValue={setRotateValue}
    btnText="Learn more"
    isStageFocus={isStageFocus}
    setIsStageFocus={setIsStageFocus}
    setDarkTheme={setDarkTheme}
    setIsIslandLightlyRotating={setIsIslandLightlyRotating}
  />,
  <InfoBox
    title="CONTACT"
    text="I am currently looking for a permanent position as a front-end developer, do not hesitate to contact me if you are interested in my profile."
    targetPosition={{ x: 300, y: 20, z: 300 }}
    targetLookAt={[-1000, 0, 400]}
    rotateValue={-22.8}
    setRotateValue={setRotateValue}
    btnText="Contact me"
    isStageFocus={isStageFocus}
    setIsStageFocus={setIsStageFocus}
    setIsIslandLightlyRotating={setIsIslandLightlyRotating}
  />,
  // add more stages as needed
];
interface InfoProps {
  currentStage: number;
  isStageFocus: boolean;
  setIsStageFocus: (focus: boolean) => void;
  setDarkTheme: (focus: boolean) => void;
  setIsIslandLightlyRotating: (focus: boolean) => void;
  setRotateValue: (value: number) => void;
  setTargetEffects: () => {
    brightness: number;
    contrast: number;
    bloomIntensity: number;
    luminanceThreshold: number;
    luminanceSmoothing: number;
    vignetteDarkness: number;
  };
}
const HomeInfo: React.FC<InfoProps> = ({
  currentStage,
  isStageFocus,
  setIsStageFocus,
  setDarkTheme,
  setIsIslandLightlyRotating,
  setRotateValue,
}) => {
  return renderContent(
    isStageFocus,
    setIsStageFocus,
    setDarkTheme,
    setIsIslandLightlyRotating,
    setRotateValue
  )[currentStage];
};

export default HomeInfo;
