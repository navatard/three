import { motion, AnimatePresence } from "framer-motion";

const SiteLoader = ({ isLoaded, onClose }) => {
  return (
    <motion.div
      transition={{ delay: 0.5 }}
      initial={{ opacity: 1 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      className="fixed inset-0 flex items-center justify-center bg-white z-50"
    >
      <motion.div
        transition={{ delay: 0.5 }}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        className="text-center"
      >
        <h1 className="text-4xl font-bold mb-4">
          Hi, I'm <span className="blue-gradient_text">Mathieu</span>
        </h1>
        <p className="pb-4">
          I am a software engineer from France,
          <br />I am delighted to present my new site to you
        </p>
        <div className="h-[40px]">
          {isLoaded && (
            <motion.button
              transition={{ delay: 0.5 }}
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              onClick={onClose}
              className="px-4 py-2 bg-blue-500 text-white rounded-lg"
            >
              Enter Site
            </motion.button>
          )}
        </div>
      </motion.div>
    </motion.div>
  );
};

export default SiteLoader;
