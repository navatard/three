import { useFrame } from "@react-three/fiber";
import { useRef, useState, useEffect } from "react";

const CameraAnimation = ({
  isEnteringSite,
  setIsEnteringSite,
  delay = 1000,
  duration = 5000,
}) => {
  const targetPosition = { x: 300, y: 300, z: 1100 };
  const [startAnimation, setStartAnimation] = useState(false);
  const startTimeRef = useRef(null);

  useEffect(() => {
    if (isEnteringSite) {
      const timer = setTimeout(() => {
        setStartAnimation(true);
        startTimeRef.current = performance.now();
      }, delay);
      return () => clearTimeout(timer);
    }
  }, [isEnteringSite, delay]);

  const easeInOutCubic = (t) => {
    return t < 0.5 ? 4 * t * t * t : 1 - Math.pow(-2 * t + 2, 3) / 2;
  };

  useFrame((state) => {
    if (startAnimation) {
      const elapsed = performance.now() - startTimeRef.current;
      const t = Math.min(elapsed / duration, 1); // Normalized time between 0 and 1
      const easedT = easeInOutCubic(t); // Apply easing function

      // Calculate the new camera position based on the eased time
      state.camera.position.lerpVectors(
        { x: 2000, y: 2000, z: 1500 }, // Start position
        targetPosition,
        easedT
      );
      state.camera.lookAt(0, 0, 0); // Ensure camera is oriented towards the center
      state.camera.updateProjectionMatrix();

      // Log the camera position and properties

      // Check if the animation is complete
      if (t === 1) {
        setStartAnimation(false);
        setIsEnteringSite(false);
      }
    }
  });

  return null;
};

export default CameraAnimation;
