declare module "./../pages" {
  import { ComponentType } from "react";
  export const Home: ComponentType;
  export const About: ComponentType;
  export const Projects: ComponentType;
  export const Contact: ComponentType;
}
