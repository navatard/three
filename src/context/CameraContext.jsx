import React, { createContext, useContext, useState, useRef } from "react";

const CameraContext = createContext();

export const CameraProvider = ({ children }) => {
  const [targetPosition, setTargetPosition] = useState(null);
  const [animationDelay, setAnimationDelay] = useState(0);
  const [targetLookAt, setTargetLookAt] = useState([0, 0, 0]);

  const setTargetPositionWithDelay = (
    position,
    lookAt = [0, 0, 0],
    delay = 0
  ) => {
    setTargetPosition(position);
    setAnimationDelay(delay);
    setTargetLookAt(lookAt);
  };

  return (
    <CameraContext.Provider
      value={{
        targetPosition,
        setTargetPosition,
        setTargetPositionWithDelay,
        animationDelay,
        targetLookAt,
      }}
    >
      {children}
    </CameraContext.Provider>
  );
};

export const useCamera = () => useContext(CameraContext);
