import { Canvas } from "@react-three/fiber";
import { useRef, useState, useEffect } from "react";
import { AnimatePresence, motion } from "framer-motion";
import SiteLoader from "../components/SiteLoader";
import CameraAnimation from "../components/CameraAnimation";
import { useCamera } from "../context/CameraContext.jsx";
import StageInfo from "../components/StageInfo";
import {
  EffectComposer,
  Bloom,
  Vignette,
  BrightnessContrast,
} from "@react-three/postprocessing";

import Navbar from "../components/Navbar";

import Island from "../models/Island";
import Sky from "../models/Sky";
import Plane from "../models/Plane";
import Contact1 from "../models/Contact1";
import Robot1 from "../models/Robot1";
import Robot2 from "../models/Robot2";
import Robot3 from "../models/Robot3";
import Badim1 from "../models/Badim1";
import Badim2 from "../models/Badim2";
import Badim3 from "../models/Badim3";
import Badim4 from "../models/Badim4";
import Spider from "../models/Spider";
import Phoenix from "../models/Phoenix";
import HomeInfo from "../components/HomeInfo";

const Home = () => {
  const [isRotating, setIsRotating] = useState(false);
  const [currentStage, setCurrentStage] = useState(1);
  const initialRotationY = -17.8;
  const [isSiteLoaded, setIsSiteLoaded] = useState(false);
  const [isContentLoaded, setIsContentLoaded] = useState(false);
  const [isEnteringSite, setIsEnteringSite] = useState(false);
  const { setTargetPositionWithDelay } = useCamera();
  const [isStageFocus, setIsStageFocus] = useState(false);
  const [darkTheme, setDarkTheme] = useState(false);
  const [isIslandLightlyRotating, setIsIslandLightlyRotating] = useState(false);
  const [rotateValue, setRotateValue] = useState(0);

  const islandRef = useRef();
  const scrollContainerRef = useRef();

  const adjustIslandForScreenSize = () => {
    let screenScale;
    let screenPosition;
    const rotation = [0.1, 4.7, 0];

    if (window.innerWidth < 768) {
      screenScale = [0.2, 0.2, 0.2]; // Smaller scale for smaller screens
      screenPosition = [0, 0, 0]; // Adjust position for smaller screens
    } else {
      screenScale = [0.3, 0.3, 0.3]; // Adjust scale for larger screens
      screenPosition = [0, 0, 0]; // Adjust position for larger screens
    }

    return [screenScale, screenPosition, rotation];
  };

  const [islandScale, islandPosition, islandRotation] =
    adjustIslandForScreenSize();

  const handleCloseLoader = () => {
    setIsSiteLoaded(true);
    setIsEnteringSite(true);
    setTargetPositionWithDelay({ x: 300, y: 300, z: 1100 }, [0, 0, 0], 1000);
  };

  useEffect(() => {
    if (isEnteringSite && !isSiteLoaded) {
      const timer = setTimeout(() => {
        setIsSiteLoaded(true);
      }, 2000); // Delay to ensure the camera animation completes
      return () => clearTimeout(timer);
    }
  }, [isEnteringSite, isSiteLoaded]);

  const [effects, setEffects] = useState({
    brightness: 0.03,
    contrast: 0.5,
    bloomIntensity: 0.1,
    luminanceThreshold: 0.3,
    luminanceSmoothing: 0,
    vignetteDarkness: 0.7,
  });

  const [targetEffects, setTargetEffects] = useState({
    brightness: 0.03,
    contrast: 0.5,
    bloomIntensity: 0.1,
    luminanceThreshold: 0.3,
    luminanceSmoothing: 0,
    vignetteDarkness: 0.7,
  });

  const animateEffects = () => {
    const keys = Object.keys(effects);
    setEffects((currentEffects) => {
      const newEffects = { ...currentEffects };
      keys.forEach((key) => {
        newEffects[key] += (targetEffects[key] - currentEffects[key]) * 0.1;
      });
      return newEffects;
    });
  };

  useEffect(() => {
    const animationFrame = requestAnimationFrame(animateEffects);
    return () => cancelAnimationFrame(animationFrame);
  }, [effects, targetEffects]);

  useEffect(() => {
    if (darkTheme) {
      setTargetEffects(() => ({
        brightness: 0,
        contrast: 0.9,
        bloomIntensity: 0,
        luminanceThreshold: 0,
        luminanceSmoothing: 0,
        vignetteDarkness: 0.7,
      }));
    } else {
      setTargetEffects(() => ({
        brightness: 0.03,
        contrast: 0.5,
        bloomIntensity: 0.1,
        luminanceThreshold: 0.3,
        luminanceSmoothing: 0,
        vignetteDarkness: 0.7,
      }));
    }
  }, [darkTheme]);

  return (
    <section className="w-full h-screen relative">
      <Navbar
        isStageFocus={isStageFocus}
        setIsStageFocus={setIsStageFocus}
        setDarkTheme={setDarkTheme}
        setIsIslandLightlyRotating={setIsIslandLightlyRotating}
        setRotateValue={setRotateValue}
      />
      <AnimatePresence>
        {!isSiteLoaded && (
          <SiteLoader isLoaded={isContentLoaded} onClose={handleCloseLoader} />
        )}
      </AnimatePresence>

      <AnimatePresence>
        {currentStage && (
          <motion.div
            transition={{ delay: 0 }}
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: 20 }}
          >
            <StageInfo
              currentStage={currentStage}
              isStageFocus={isStageFocus}
              setIsStageFocus={setIsStageFocus}
              setIsIslandLightlyRotating={setIsIslandLightlyRotating}
              setDarkTheme={setDarkTheme}
            />
          </motion.div>
        )}
      </AnimatePresence>

      <div className="absolute top-56 left-0 right-56 z-20 flex items-center justify-end">
        <AnimatePresence>
          {currentStage && !isStageFocus && (
            <motion.div
              transition={{ delay: 0 }}
              initial={{ opacity: 0, y: 20 }}
              animate={{ opacity: 1, y: 0 }}
              exit={{ opacity: 0, y: 20 }}
            >
              <HomeInfo
                currentStage={currentStage}
                isStageFocus={isStageFocus}
                setIsStageFocus={setIsStageFocus}
                setDarkTheme={setDarkTheme}
                setIsIslandLightlyRotating={setIsIslandLightlyRotating}
                setRotateValue={setRotateValue}
              />
            </motion.div>
          )}
        </AnimatePresence>
      </div>

      <div
        ref={scrollContainerRef}
        className={`${
          isStageFocus ? "overflow-hidden" : "overflow-scroll"
        } h-[200vh]  absolute left-0 top-0 w-full z-10 scroll-container`} //${isStageFocus ? "overflow-hidden" : "overflow-scroll"}
      >
        <div style={{ height: "1800vh" }}>
          {/* Content to ensure scrolling */}
        </div>
      </div>
      <Canvas
        className={`w-full h-screen bg-transparent ${
          isRotating ? "cursor-grabbing" : "cursor-grab"
        }`}
        camera={{
          position: [1500, 1500, 2000], //position: [1500, 1500, 2000],
          fov: 110,
          near: 0.1,
          far: 10000,
        }} // Further adjusted initial position and FOV
        onCreated={() => setIsContentLoaded(true)}
      >
        <CameraAnimation duration={5000} />
        <directionalLight
          position={[200, 200, 200]}
          intensity={2} //0.5
        />
        <ambientLight intensity={3} />
        <pointLight />
        <spotLight />
        <hemisphereLight
          skyColor="#b1e1ff"
          groundColor="#000000"
          intensity={1}
        />
        <Island
          position={islandPosition}
          scale={islandScale}
          rotation={islandRotation}
          isRotating={isRotating}
          setIsRotating={setIsRotating}
          setCurrentStage={setCurrentStage}
          currentStage={currentStage}
          ref={islandRef}
          initialRotationY={initialRotationY}
          scrollContainerRef={scrollContainerRef}
          setIsStageFocus={setIsStageFocus}
          isIslandLightlyRotating={isIslandLightlyRotating}
          setIsIslandLightlyRotating={setIsIslandLightlyRotating}
          rotateValue={rotateValue}
        >
          <Robot1 islandRef={islandRef} ref={islandRef} />
          <Robot2 islandRef={islandRef} ref={islandRef} />
          <Robot3 islandRef={islandRef} ref={islandRef} />

          <Badim1 islandRef={islandRef} ref={islandRef} />
          <Badim2 islandRef={islandRef} ref={islandRef} />
          <Badim3 islandRef={islandRef} ref={islandRef} />
          <Badim4 islandRef={islandRef} ref={islandRef} />

          <Spider islandRef={islandRef} ref={islandRef} />

          <Phoenix />

          <Contact1 islandRef={islandRef} ref={islandRef} />
        </Island>
        <Plane />
        <Sky />
        <EffectComposer>
          <BrightnessContrast
            brightness={effects.brightness}
            contrast={effects.contrast}
          />
          <Bloom
            intensity={effects.bloomIntensity}
            luminanceThreshold={effects.luminanceThreshold}
            luminanceSmoothing={effects.luminanceSmoothing}
          />
          <Vignette
            eskil={false}
            offset={0}
            darkness={effects.vignetteDarkness}
          />
        </EffectComposer>
      </Canvas>
    </section>
  );
};

export default Home;
